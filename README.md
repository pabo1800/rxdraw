# Usage

The project has dependencies declared in the gradle build script. The easiest
way to run the program is to use gradle. Either run `gradlew run` or
`gradlew.bat /run` in your terminal or import the project as a gradle project in
your IDE. Add a gradle run configuration with the argument run.
