package se.miun.dt076g.rxdraw.drawables;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.canvas.Canvas;

public class Clear implements Drawable {
    public void draw(GraphicsContext ctx) {
        final var canvas = ctx.getCanvas();
        final var w = canvas.getWidth();
        final var h = canvas.getHeight();
        ctx.clearRect(0, 0, w, h);
    }
}
