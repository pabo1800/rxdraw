package se.miun.dt076g.rxdraw.drawables;

import javafx.scene.canvas.GraphicsContext;

public interface Drawable {
    public void draw(GraphicsContext ctx);
}
