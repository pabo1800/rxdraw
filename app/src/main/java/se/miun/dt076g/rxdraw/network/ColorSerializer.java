package se.miun.dt076g.rxdraw.network;

import javafx.scene.paint.Color;
import com.esotericsoftware.kryo.*;
import com.esotericsoftware.kryo.io.*;

public class ColorSerializer extends Serializer<Color> {

   public void write (Kryo kryo, Output output, Color color) {
      output.writeDouble(color.getRed());
      output.writeDouble(color.getGreen());
      output.writeDouble(color.getBlue());
      output.writeDouble(color.getOpacity());
   }

   public Color read(Kryo kryo, Input input, Class<Color> type) {
       double red = input.readDouble();
       double green = input.readDouble();
       double blue = input.readDouble();
       double opacity = input.readDouble();
      return new Color(red, green, blue, opacity);
   }
}
