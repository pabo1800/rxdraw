package se.miun.dt076g.rxdraw.drawables;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Oval implements Shape {

    Point2D start = null;
    Point2D end = null;
    Color color;
    Double linesize;

	public Oval() {
    }

    public Oval(Color c, Double sz) {
        this.color = c;
        this.linesize = sz;
    }

    public void addPoint(Point2D p) {
        if (start == null) {
            start = p;
        } else {
            end = p;
        }
    }

    public double x() {
        return Math.min(start.getX(), end.getX());
    }

    public double y() {
        return Math.min(start.getY(), end.getY());
    }

    private Double width() {
        return Math.abs(end.getX() - start.getX());
    }

    private Double height() {
        return Math.abs(end.getY() - start.getY());
    }

    public void draw(GraphicsContext ctx) {
        if (start == null || end == null) {
            return;
        }
        ctx.setLineWidth(linesize);
        ctx.setStroke(color);
        ctx.strokeOval(x(), y(), width(), height());
    }

    public String toString() {
        return "Oval[" + start + ", " + end + "]";
    }
}
