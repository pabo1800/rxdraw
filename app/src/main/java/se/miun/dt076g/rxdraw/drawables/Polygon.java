package se.miun.dt076g.rxdraw.drawables;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Polygon implements Shape {

    List<Point2D> points = new ArrayList<Point2D>();
    Color color;
    Double linesize;

	public Polygon() {
    }

    public Polygon(Color c, Double sz) {
        this.color = c;
        this.linesize = sz;
    }

    public void draw(GraphicsContext ctx) {
        ctx.setLineWidth(linesize);
        ctx.setStroke(color);

        lines().subscribe(points ->  {
            if (points.size() > 1) {
                final var from = points.get(0);
                final var to = points.get(1);
                ctx.strokeLine(from.getX(), from.getY(), to.getX(), to.getY());
            }
        });
    }

    private Observable<List<Point2D>> lines() {
        return Observable.fromIterable(points)
            .buffer(2, 1);
    }

    public void addPoint(Point2D p) {
        points.add(p);
    }

    public String toString() {
        return points.toString();
    }
}
