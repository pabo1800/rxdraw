package se.miun.dt076g.rxdraw.network;

import java.util.ArrayList;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.*;

import se.miun.dt076g.rxdraw.drawables.*;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;

public class Network {

    public static void register(EndPoint ep) {
        Kryo kryo = ep.getKryo();
        kryo.addDefaultSerializer(Color.class, ColorSerializer.class);
        kryo.addDefaultSerializer(Point2D.class, Point2DSerializer.class);
        kryo.register(ArrayList.class);
        kryo.register(Drawable.class);
        kryo.register(Clear.class);
        kryo.register(Color.class);
        kryo.register(Point2D.class);
        kryo.register(Shape.class);
        kryo.register(Line.class);
        kryo.register(Polygon.class);
        kryo.register(Rectangle.class);
        kryo.register(Oval.class);
        kryo.register(DrawSettings.class);
        kryo.register(DrawSettings.class);
    }
}
