package se.miun.dt076g.rxdraw.drawables;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Line implements Shape {

    Point2D start = null;
    Point2D end = null;
    Color color;
    Double linesize;

    private Line() {
    }

    public Line(Color c, Double sz) {
        this.color = c;
        this.linesize = sz;
    }

    public void addPoint(Point2D p) {
        if (start == null) {
            start = p;
        } else {
            end = p;
        }
    }

    public void draw(GraphicsContext ctx) {
        if (start == null || end == null) {
            return;
        }
        ctx.setLineWidth(linesize);
        ctx.setStroke(color);
        ctx.strokeLine(start.getX(), start.getY(), end.getX(), end.getY());
    }

    public String toString() {
        return "Line[" + start + ", " + end + "]";
    }
}
