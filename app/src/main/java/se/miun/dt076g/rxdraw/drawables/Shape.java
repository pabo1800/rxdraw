package se.miun.dt076g.rxdraw.drawables;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;

public interface Shape extends Drawable {
    public void draw(GraphicsContext ctx);
    public void addPoint(Point2D p);
    public String toString();
}
