package se.miun.dt076g.rxdraw.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subjects.ReplaySubject;
import io.reactivex.rxjava3.subjects.Subject;
import se.miun.dt076g.rxdraw.drawables.Clear;
import se.miun.dt076g.rxdraw.drawables.Drawable;
import se.miun.dt076g.rxdraw.drawables.Shape;

public class RxDrawServer {

    Server server = null;
    Subject<Drawable> drawables;
    List<Subscription> subscriptions = new ArrayList<>();
    boolean connected = false;

    public RxDrawServer(int TCPPort) {
        drawables = ReplaySubject.create();
        initServer();
    }

    void initServer() {
        try {
            server = new Server(16384, 16384);
            Network.register(server);
            addServerListener();
            server.start();
            server.bind(5555);
            connected = true;
            System.out.println("Server:  Connected");
        } catch (IOException e) {
            connected = false;
            System.out.println("Server ERROR: " + e.getMessage());
        }
    }

    void addServerListener() {
        server.addListener(new Listener() {

            @Override
            public void connected(Connection connection) {
                var addr = connection.getRemoteAddressTCP();
                System.out.println("Server: Received new connection (" + addr + ")");
                add(connection);
            }

            @Override
            public void disconnected(Connection connection) {
                System.out.println("Server: Client disconnected");
                remove(connection);
            }

            @Override
            public void received(Connection connection, Object obj) {
                if (obj instanceof Drawable) {
                    System.out.println("Server: Received a shape");
                    if (obj instanceof Clear) {
                        reset();
                    }
                    drawables.onNext((Drawable) obj);
                }
            }
        });
    }

    public void reset() {
        drawables = ReplaySubject.create();
        subscriptions().subscribe(sub -> {
            sub.unsubscribe();
            sub.subscribe(drawables);
        });
        System.out.println("Server: did reset");
    }

    void add(Connection connection) {
        final var sub = new Subscription(connection);
        sub.subscribe(drawables);
        subscriptions.add(sub);
    }

    void remove(Connection connection) {
        final var match = new Subscription(connection);
        final var idx = subscriptions.indexOf(match);
        if (idx != -1) {
            System.out.println("Removing a subscription");
            final var sub = subscriptions.get(idx);
            sub.unsubscribe();
            subscriptions.remove(idx);
        }
    }

    private Observable<Subscription> subscriptions() {
        return Observable.fromIterable(subscriptions);
    }

    public void stop() {
        if (server != null) {
            server.stop();
        }
    }

    public boolean isConnected() {
        return connected;
    }

    private class Subscription {
        Disposable disposable;
        Connection connection;

        Subscription(Connection connection) {
            this.connection = connection;
        }

        void unsubscribe() {
            disposable.dispose();
        }

        void subscribe(Observable<Drawable> source) {
            disposable = source.subscribe(connection::sendTCP);
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + getEnclosingInstance().hashCode();
            result = prime * result + ((connection == null) ? 0 : connection.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Subscription other = (Subscription) obj;
            if (!getEnclosingInstance().equals(other.getEnclosingInstance()))
                return false;
            if (connection == null) {
                if (other.connection != null)
                    return false;
            } else if (!connection.equals(other.connection))
                return false;
            return true;
        }

        private RxDrawServer getEnclosingInstance() {
            return RxDrawServer.this;
        }
    }

}
