package se.miun.dt076g.rxdraw.drawables;

import javafx.scene.paint.Color;

public class DrawSettings {
    public Color color;
    public Double linesize;
    public String shape;

	public DrawSettings() {
    }

	public DrawSettings(Color color, Double linesize, String shape) {
		this.color = color;
		this.linesize = linesize;
		this.shape = shape;
	}
}
