package se.miun.dt076g.rxdraw.controllers;

import se.miun.dt076g.rxdraw.drawables.*;
import se.miun.dt076g.rxdraw.network.*;

import com.esotericsoftware.kryonet.*;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Function;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import io.reactivex.rxjavafx.observables.JavaFxObservable;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;

public class RxDrawController implements Initializable {

    @FXML
    public VBox canvasParent;
    @FXML
    private Canvas canvas;
    @FXML
    private GraphicsContext ctx;
    @FXML
    private ColorPicker colorPicker;
    @FXML
    private ToggleGroup shapeBtns;
    @FXML
    private Slider lineThickness;
    @FXML
    private Button clearBtn;

    @FXML
    private VBox networkPanel;

    @FXML
    private HBox connectPanel;
    @FXML
    private Button serverBtn;
    @FXML
    private Button clientBtn;

    @FXML
    private HBox disconnectPanel;
    @FXML
    private Button disconnectBtn;


    Observable<Color> color;
    Observable<Double> strokeSize;
    Observable<String> shapeType;
    Observable<Boolean> leftButtonDown;
    Observable<Boolean> leftButtonUp;
    Observable<MouseEvent> mouse;
    Observable<ActionEvent> clearEvent;

    List<Drawable> drawables = new ArrayList<>();
    Shape shape = null;
    DrawSettings settings = null;
    Subject<Shape> newshape = PublishSubject.create();

    RxDrawClient client = null;
    RxDrawServer server = null;

    @Override
    @FXML
    public void initialize(URL url, ResourceBundle rb) {
        setup();

        showConnectPanel();

        clearEvent = JavaFxObservable
            .eventsOf(clearBtn, ActionEvent.ACTION);
        clearEvent.subscribe(this::onClear);

        JavaFxObservable
            .eventsOf(clientBtn, ActionEvent.ACTION)
            .subscribe(evt -> {
                clearLocal();
                if (startClient()) {
                    showDisconnectPanel();
                }
            });

        JavaFxObservable
            .eventsOf(serverBtn, ActionEvent.ACTION)
            .subscribe(evt -> {
                clearLocal();
                if (startServer() && startClient()) {
                    showDisconnectPanel();
                } else {
                    stopNetworking();
                }
            });

        JavaFxObservable
            .eventsOf(disconnectBtn, ActionEvent.ACTION)
            .subscribe(evt -> {
                stopNetworking();
                showConnectPanel();
            });

        // Combine settings observables into one
        // When the settings is changed, update the stored settings
        // and the current shape
        Observable
            .combineLatest(color, strokeSize, shapeType, DrawSettings::new)
            .subscribe(set -> { this.settings = set; startNewShape();});

        // Configure mouse actions.
        // On mouse down, start the new shape, add it to the list so that
        // it gets drawn.
        // On mouse up finish the shape and start a new shape.
        leftButtonDown
            .subscribe(evt -> { drawables.add(shape); });
        leftButtonUp
            .subscribe(evt -> { finishShape(); });

        Observable<Point2D[]> paint = mouse
            .buffer(2, 1)
            .map(this::eventPairToPoints)
            .window(leftButtonDown, b -> leftButtonUp)
            .flatMap(x -> x);

        paint
            .doOnNext(ps -> shape.addPoint(ps[1]))
            .doOnError(System.err::println)
            .subscribe(ps -> redraw());

    }

    private void showConnectPanel() {
        networkPanel.getChildren().remove(disconnectPanel);
        networkPanel.getChildren().remove(connectPanel);
        networkPanel.getChildren().add(connectPanel);
    }

    private void showDisconnectPanel() {
        networkPanel.getChildren().remove(disconnectPanel);
        networkPanel.getChildren().remove(connectPanel);
        networkPanel.getChildren().add(disconnectPanel);
    }

    private boolean startServer() {
            server = new RxDrawServer(5555);
            return server.isConnected();
    }

    private boolean startClient() {
        if (client != null) {
            client.stop();
        }
        client = new RxDrawClient();
        client.received()
            .subscribe(obj -> {
                if (obj instanceof Clear) {
                    clearLocal();
                    return;
                }
                if (obj instanceof Shape) {
                    Shape recvd = (Shape) obj;
                    System.out.println("Client: Received a shape");
                    System.out.println("\t" + recvd);
                    drawables.add(recvd);
                    redraw();
                }
            });

        newshape.subscribe(client);
        clearEvent.map(evt -> { return new Clear(); }).subscribe(client);
        return client.isConnected();
    }

    private void finishShape() {
        // Ironically finishing the current shape means starting a new shape.
        // send shape to server
        newshape.onNext(shape);
        startNewShape();
    }

    private void startNewShape() {
        switch (settings.shape) {
            case "Rectangle":
                shape = new Rectangle(settings.color, settings.linesize);
                break;
            case "Freehand":
                shape = new Polygon(settings.color, settings.linesize);
                break;
            case "Line":
                shape = new Line(settings.color, settings.linesize);
                break;
            case "Oval":
                shape = new Oval(settings.color, settings.linesize);
                break;
            default: throw new RuntimeException("Invalid shapetype");
        }
    }

    private Point2D[] eventPairToPoints(List<MouseEvent> evts) {
        final var first = evts.get(0);
        final var second = evts.get(1);
        return new Point2D[] {
            new Point2D(first.getX(), first.getY()),
            new Point2D(second.getX(), second.getY()),
        };
    }

    /* Adapted from
     * https://github.com/headinthebox/RxPaint/blob/master/src/Paint.java
     */
    <T> Observable<T> fromEvent(Node node, EventType<? extends Event> event, Function<? super Event, ? extends T> selector) {
        Observable<T> source = JavaFxObservable.eventsOf(node, event)
            .map(e -> selector.apply(e));
        return source;
    }

    Observable<Boolean> leftButtonDown(Canvas canvas) {
        return fromEvent(canvas, MouseEvent.MOUSE_PRESSED, event -> true);
    }

    Observable<Boolean> leftButtonUp(Canvas canvas) {
        return fromEvent(canvas, MouseEvent.MOUSE_RELEASED, event -> false);
    }

    Observable<MouseEvent> mouseMoves(Canvas canvas) {
        return fromEvent(canvas, MouseEvent.MOUSE_MOVED, event -> (MouseEvent)event);
    }

    Observable<MouseEvent> mouseDrags(Canvas canvas) {
        return fromEvent(canvas, MouseEvent.MOUSE_DRAGGED, event -> (MouseEvent) event);
    }

    Observable<String> shape() {
        return JavaFxObservable.valuesOf(shapeBtns.selectedToggleProperty(),
                shapeBtns.selectedToggleProperty().getValue())
            .cast(ToggleButton.class)
            .map(ToggleButton::getText);
    }

    Observable<Double> lineSize() {
        return JavaFxObservable.valuesOf(lineThickness.valueProperty(),
                lineThickness.getValue())
            .map(prop -> prop.doubleValue())
            .filter(value -> value % lineThickness.getMajorTickUnit() == 0);
    }

    private void redraw() {
        clearCanvas();
        Observable.fromIterable(drawables)
            .subscribe(shape -> shape.draw(ctx));
    }

    private void setup() {
        ctx = canvas.getGraphicsContext2D();
        ctx.setLineCap(StrokeLineCap.ROUND);
        clearCanvas();

        if (canvasParent == null) {
            throw new RuntimeException("Canvas parent was null!");
        }
        canvas.widthProperty().bind(canvasParent.widthProperty());
        canvas.heightProperty().bind(canvasParent.heightProperty());

        color = JavaFxObservable
            .eventsOf(colorPicker, ActionEvent.ACTION)
            .map(evt -> colorPicker.getValue())
            .startWithItem(colorPicker.getValue());

        strokeSize = JavaFxObservable
            .valuesOf(lineThickness.valueProperty(), lineThickness.getValue())
            .map(prop -> prop.doubleValue())
            .filter(value -> value % lineThickness.getMajorTickUnit() == 0);

        shapeType = JavaFxObservable
            .valuesOf(shapeBtns.selectedToggleProperty(),
                      shapeBtns.selectedToggleProperty().getValue())
            .cast(ToggleButton.class)
            .map(ToggleButton::getText);

        leftButtonDown = leftButtonDown(canvas);
        leftButtonUp = leftButtonUp(canvas);
        mouse = Observable.merge(mouseMoves(canvas), mouseDrags(canvas));
    }

    public void onClear(ActionEvent evt) {
        clearLocal();
    }

    public void clearLocal() {
        drawables.clear();
        clearCanvas();
    }

    private void clearCanvas() {
        ctx.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }

    public void stopNetworking() {
        System.out.println("Stopping networks..");
        if (server != null) {
            server.stop();
        } else {
            System.out.println("No server");
        }
        if (client != null) {
            client.stop();
        } else {
            System.out.println("No client");
        }
        System.out.println("Done!");
    }

    public void onExit() {
        stopNetworking();
        Platform.exit();
    }
}
