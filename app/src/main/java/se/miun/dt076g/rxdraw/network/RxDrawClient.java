package se.miun.dt076g.rxdraw.network;

import se.miun.dt076g.rxdraw.drawables.*;

import java.io.IOException;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;

import com.esotericsoftware.kryonet.*;

public class RxDrawClient implements Observer<Drawable> {
    static final int CONNECTION_TIMEOUT = 5000;

    Client client;
    Subject<Drawable> received = PublishSubject.create();

    public RxDrawClient() {
        init();

        client.addListener(new Listener()  {
            @Override
            public void received(Connection connection, Object obj) {
                if (obj instanceof Drawable) {
                    received.onNext((Drawable) obj);
                }
            }
        });

    }

    void init() {
        try {
            client = new Client(16384, 16384);
            Network.register(client);
            client.start();
            client.connect(CONNECTION_TIMEOUT, "127.0.0.1", 5555);
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    public void stop() {
        if (client != null) {
            client.stop();
        }
    }

    public boolean isConnected() {
        return client.isConnected();
    }

    private void send(Object obj) {
        client.sendTCP(obj);
    }

    public Observable<Drawable> received() {
        return received.hide();
    }

    @Override
    public void onSubscribe(@NonNull Disposable d) {
    }

    @Override
    public void onNext(@NonNull Drawable drawable) {
        send(drawable);
    }

    @Override
    public void onError(@NonNull Throwable e) {
        System.err.println("Client ERROR: " + e);
    }

    @Override
    public void onComplete() {
    }
}
