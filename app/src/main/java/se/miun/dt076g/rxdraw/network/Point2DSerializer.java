package se.miun.dt076g.rxdraw.network;

import javafx.geometry.Point2D;
import com.esotericsoftware.kryo.*;
import com.esotericsoftware.kryo.io.*;

public class Point2DSerializer extends Serializer<Point2D> {

   public void write (Kryo kryo, Output output, Point2D point) {
      output.writeDouble(point.getX());
      output.writeDouble(point.getY());
   }

   public Point2D read(Kryo kryo, Input input, Class<Point2D> type) {
       double x = input.readDouble();
       double y = input.readDouble();
      return new Point2D(x, y);
   }
}
